# 前言

考虑到这已经是这个软件的第三版了，前两版都不再兼容和维护。这次我发誓会继续维护此版本。
此版本的优势：

- 可以完全本地运行或者网络运行。
- 软件小：1mb。
- 软件占用运行内存小：30mb 左右

# 简介

快捷键助手是一款帮助您记忆并提示“正在使用的软件”的快捷键的应用。随着我们使用的电脑软件越来越多，CAD、Vistual Studio、OneNote、Word、WPS、QQ、Mathpix、utools、VSCode、Blender、Solidworks、PS、AI 等等软件的快捷键混杂在一起很难记忆。
小白在学习新软件的时候也总要花费很长时间来记忆快捷键，加上在我最初入坑 MacOS 的时候就被上面类似的软件”CheatSheet”深深吸引了。
![图1](https://github.com/BetterWorld-Liuser/XiaoChao/blob/master/picture/%E5%9B%BE%E7%89%871.png)

![图2](https://github.com/BetterWorld-Liuser/XiaoChao/blob/master/picture/%E5%9B%BE%E7%89%872.png)

所以就本软件就诞生啦。

# 已添加快捷键软件列表

- Windows 常用快捷键
- OneNote
- Word
- Edge
- 知乎网页版
- Adobe PhotoShop
- CAD
- Adobe illustrator
- 3dsMax
- SketchUp
- notion
- WPS

# 安装说明

直接安装

# 使用方式

软件默认设置开机启动，可以用 Windows 自带的开机管理器管理，之后使用 Alt+S (可自定义) 快捷键来关闭和呼出，软件支持夜间模式。

# 如何深度使用

1. 本软件目录下有 data 文件夹, 其中储存快捷键数据, 文件的名称及为匹配的名称. 比如如果需要匹配 WPS, 可以创建名为 wps.md 的文件. 文件格式显而易见. 以 ‘# ‘ 开头的是标题, 其余数据以空格作为分割.
2. 本软件没有识别不同软件快捷的黑科技，所有快捷键数据都需要人工填写，不过，默认的订阅地址已存在了市面上比较常用软件的快捷键。
3. 目前支持的设置选项如下:

开机启动 是/否
背景色 #??????
字体颜色 #??????
装饰色 #??????
快捷键 alt+s
透明度 0-100

默认订阅地址为了保证国内的快速访问（gitee）：

https://gitee.com/liuzhengdong666/ShortCutHelper/raw/master/subscription.json

开源地址（优先更新）：

https://github.com/liuzhengdong2/ShortCutHelper

# 如何删除本软件

本软件不包含任何注册表操作，直接删除文件即可。

# 已知缺陷

- 声明一下，我想把“快捷键助手”做成一个系统插件形式的软件，所以在系统托盘里目前不计划有图标显示（除非你们都在骂）。

# 捐赠

如果你想快一点实现自己的需求，可以找作者定制。如果你用爽了也能捐赠给作者，啦啦啦~
请备注自己的联系方式哟~
当前捐赠人数：3 人
当前捐赠总额：260 元

| 微信                                                                                                      | 支付宝                                                                                                      |
| --------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| ![微信二维码](https://github.com/BetterWorld-Liuser/XiaoChao/blob/master/picture/%E5%9B%BE%E7%89%875.jpg) | ![支付宝二维码](https://github.com/BetterWorld-Liuser/XiaoChao/blob/master/picture/%E5%9B%BE%E7%89%876.png) |
